const express          = require('express');
const bodyparser       = require('body-parser');
// const port          = 8070;
const config           = require('./../config').server;
const NetworkSpeed     = require('network-speed');
const testNetworkSpeed = new NetworkSpeed();
var server             = require('http').Server(express);
var io                 = require('socket.io')(server);

const app     = express();
const routes  = require('./routes');

app.use(express.json());
app.use(routes);
app.use(function(req,res,next){
	req.io = io;
	next();
});

async function getNetworkSpeed() {

    try {
        const baseUrl = 'http://eu.httpbin.org/stream-bytes/50000000';
        const fileSize = 500000;
        const downloadSpeed = await testNetworkSpeed.checkDownloadSpeed(baseUrl, fileSize);
    
        const options = {
          hostname: 'www.popbox.asia'
        };
        const uploadSpeed = await testNetworkSpeed.checkUploadSpeed(options);
        console.log(uploadSpeed, downloadSpeed);
        var results = downloadSpeed;

	    io.sockets.emit('connections', results);

    } catch (error) {
        console.log(error.message);        
    }

}

// An interval to 
setInterval(function() {  
    getNetworkSpeed();
}, 3000);

io.on('connections', function (socket) {
	var results = { bps: '0', kbps: '0', mbps: '0' }
	socket.emit('connections', results);
});


server.listen(config.port, config.host, () => {
    console.log('listening ' + config.port);
});